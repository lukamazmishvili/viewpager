package com.example.view_pager.Fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.view_pager.R

class FirstFragment : Fragment(R.layout.fragment_first) {

    private lateinit var noteEditText : EditText
    private lateinit var buttonADD : Button
    private lateinit var textView : TextView
    private lateinit var sharedPreferences : SharedPreferences

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        noteEditText = view.findViewById((R.id.noteEditText))
        buttonADD = view.findViewById(R.id.buttonADD)
        textView = view.findViewById(R.id.textView)

        registerListener()

        val text = sharedPreferences.getString("NOTES", "")
        textView.text = text

    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        sharedPreferences = context.getSharedPreferences("MY_APP", Context.MODE_PRIVATE)
    }

    private fun registerListener() {
        buttonADD.setOnClickListener {

            val text = textView.text.toString()
            val note = noteEditText.text.toString()
            val result = text + "\n" + note

            textView.text = result

            sharedPreferences.edit()
                .putString("NOTES", result)
                .apply()
        }
    }

}