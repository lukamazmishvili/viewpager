package com.example.view_pager

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.viewpager2.widget.ViewPager2
import com.example.view_pager.Adapters.ViewPagerFragmentAdapter
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : AppCompatActivity() {

    private lateinit var tabs : TabLayout
    private lateinit var viewPager : ViewPager2
    private lateinit var viewPagerFragmentAdapter: ViewPagerFragmentAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        viewPager.adapter = viewPagerFragmentAdapter

        TabLayoutMediator(tabs, viewPager){tab, position ->

            tab.text = "Tab ${position + 1}"
        }.attach()

    }

    private fun init(){

        tabs = findViewById(R.id.tabs)
        viewPager = findViewById(R.id.viewPager)
        viewPagerFragmentAdapter = ViewPagerFragmentAdapter(this)

    }
}